import { Selector, ClientFunction } from 'testcafe';

fixture `New Fixture`
    .page `https://dribbble.com/`;

const A = Selector('a');
const P = Selector('p');
const H3 = Selector('h3');
const Image = Selector('.dribbble-over');
const Image1 = Image.nth(0);
const URL = ClientFunction(() => {
        return window.location.href
    });

test('ImagesNavigation', async t => {
    await t
        .click(A.withText('Animation'))
        .expect(URL()).contains("/animation")
        .click(A.withText('Branding'))
        .expect(URL()).contains("/branding")
        .click(A.withText('Illustration'))
        .expect(URL()).contains("/illustration")
        .click(A.withText('Mobile'))
        .expect(URL()).contains("/mobile")
        .click(A.withText('Print'))
        .expect(URL()).contains("/print")
        .click(A.withText('Product Design'))
        .expect(URL()).contains("/product-design")
        .click(A.withText('Typography'))
        .expect(URL()).contains("/typography")
        .click(A.withText('Web Design'))
        .expect(URL()).contains("/web-design");
});

test('PageNavigation', async t => {
    await t
        .click(Selector('#t-players').find('a').withText('Designers'))
        .expect(URL()).contains("/designers")
        .click(A.withText('Teams'))
        .expect(URL()).contains("/teams")
        .hover(Selector('body').find('div').find('div').find('ul').find('li').nth(6).find('a'))
        .click(Selector('strong').withText('Courtside'))
        .expect(URL()).contains("/stories")
        .hover(Selector('.has-sub'))
        .click(Selector('#t-community').find('strong').withText('Warm-Up'))
        .expect(URL()).contains("/shots")
        .click(Selector('#t-jobs').find('a').withText('Jobs'))
        .expect(URL()).contains("/jobs");
});

test('ImagesFilters', async t => {
    const Downloads = Selector('.form-btn.outlined.btn-dropdown-link[data-name="Downloads"][data-dropdown-state="closed"]');
    const MadeWith = Selector('.form-btn.outlined.btn-dropdown-link[data-name="Made With"][data-dropdown-state="closed"]');
    const Ghost = Selector('#screenshot-7560854').find('.dribbble-over')
    await t
        .click(Selector('span').withText('Filters'))
        .typeText(Selector('#tag'), 'ghost')
        .pressKey('enter')
        .hover(Ghost)
        .expect(Ghost.textContent).contains("Ghost 👻")
        .click(Selector('#color'))
        .click(Selector('#color-4030e8'))
        .hover(Selector('#screenshot-7533191').find('.dribbble-over'))
        .expect(Selector('p').withText('Dead Slime Font Familly + 50 Illustrations').textContent).eql("Dead Slime Font Familly + 50 Illustrations")
        .click(Selector('.form-btn.outlined.btn-dropdown-link[data-name="Timeframe"][data-dropdown-state="closed"]'))
        .click(A.withText('This Past Week'))
        .click(MadeWith)
        .click(Selector('[data-param="madeWith"][data-value="adobe-xd"]'))
        .click(MadeWith)
        .click(Selector('[data-param="madeWith"][data-value="figma"]'))
        .click(MadeWith)
        .click(Selector('[data-param="madeWith"][data-value="sketch"]'))
        .click(MadeWith)
        .click(A.withText('Unsplash'))
        .expect(H3.withText('No results found').textContent).eql("No results found")
        .click(Selector('.clear-filter[data-param="madeWith"]'))
        .expect(Selector('.dribbble-over').textContent).contains("Dead Slime Font Familly")
        .click(Downloads)
        .click(Selector('[data-value=".sketch"]').find('a').withText('Sketch'))
        .click(Downloads)
        .click(A.withText('Invision Studio'))
        .click(Downloads)
        .click(Selector('[data-value=".fig"]').find('a').withText('Figma'))
        .expect(P.withText('Material backdrop UI templates').textContent).eql("Material backdrop UI templates")
        .click(Selector('.clear-filter[data-param="source_file"]'))
        .expect(P.withText('The Gaming Mountain').textContent).eql("The Gaming Mountain");
});

test('ImagesSize', async t => {
    const Height = Image1.getBoundingClientRectProperty('height');
    const Size = Selector('.icon.shot-size-toggle');
    const Icon = Selector('.icon');
    await t
        .click(Size)
        .click(Icon.nth(11).find('g').find('rect'))
        .expect(Height).lte(161)
        .click(Size)
        .click(Icon.nth(12).find('g').find('rect'))
        .expect(Height).gte(250)
        .click(Size)
        .click(Icon.nth(13).find('rect'))
        .expect(Height).lte(160)
        .click(Size)
        .click(Icon.nth(14).find('rect'))
        .expect(Height).gte(250);
});

test('LikeSave', async t => {
    const SignUp = A.withText('Sign up with Google');
    const Comunicate = Selector('#signup-overlay').find('div').find('div').find('div').find('p').textContent;
    const Close = Selector('#signup-overlay').find('[alt="close"]')
    await t
        .hover(Image1)
        .click(A.withText('Save').nth(0))
        .expect(Comunicate).eql("To save a shot, please create an account.")
        .expect(SignUp()).ok()
        .click(Close)
        .hover(Image1)
        .click(A.withText('Like').nth(0))
        .expect(Comunicate).eql("To like a shot, please create an account.")
        .expect(SignUp()).ok()
        .click(Close);
});